﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//子クラス移動するエフェクト
public class MoveParticleController : ParticleController {
    public float speedX = 30.0f;
    public float speedY = 0;
    public float speedZ = 30.0f;
    Rigidbody rigid3D;
	void Start () {
        //RigidBodyを取得
        this.rigid3D = GetComponent<Rigidbody>();
	}
	void Update () {
        //キーボードでx方向に力を加える
        float x = Input.GetAxis("Horizontal") * speedX;
        //キーボードでz方向に力を加える
        float z = Input.GetAxis("Vertical") * speedZ;
        //移動させる
        rigid3D.AddForce(x, 0, z);
	}
    private void OnCollisionEnter(Collision collision)
    {
        //地面以外のオブジェクトに触れたらエフェクトが発生する
        if(collision.gameObject.name != "Stage")
        {
            GenerateParticle();
        }
    }
}
