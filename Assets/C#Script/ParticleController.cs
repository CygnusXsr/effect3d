﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//抽象クラス　パーティクル処理コントローラー
public abstract class ParticleController : MonoBehaviour
{
    //[SerializeField]をフィールドにそれぞれ書くとprivate変数をUnity上でpublic変数のように扱える
    [SerializeField]
    private GameObject particlePrefab;
    //コントローラー位置
    [SerializeField]
    private Vector3 controllerPos;
    //パーティクル生成
    public void GenerateParticle()
    {
        //パーティクルエフェクトを生み出す
        //as GameObjectはGameObject型にキャストしている
        //Instantiate()は引数1個でもいい
        GameObject pp = Instantiate(particlePrefab) as GameObject;
        //コントローラーの位置にパーティクルを置く
        pp.transform.position = controllerPos;
        //エフェクト発生後パーティクル削除
        Destroy(pp, 2.0f);
    }
    void Start()
    {
        //エフェクトコントローラーの初期位置設定
        controllerPos = gameObject.transform.position;
    }
}
